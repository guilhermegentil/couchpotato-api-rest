/**
 * Basic API test endpoints
 *
 * @author Guilherme Gentil do Amaral
 */
var logger = require('../utils/logger');
var express = require('express');
var passport = require('passport');

var auth = require('../controllers/authController')

var router = express.Router();

// Related controller
var apiController = require('../controllers/apiController');

// Routes mapping
router.route('/')
    .get(function(req, res) {
        res.json({message: "API server is working!"})
    });

router.route('/controller')
    .get(auth.isAuthenticated, apiController.getTest);

module.exports = router;
