/**
 * Passport authentication strategies
 *
 * @author Guilherme G. Amaral (ggamaral@gmail.com)
 */
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;

//Hardcoded username/password for test
var hardcodedUsername = "test";
var hardcodedPassword = "test";

// Authentication
passport.use(new BasicStrategy(
    function(username, password, done) {
        if(username == hardcodedUsername && password == hardcodedPassword) {
            return done(null, {username: username});
        }
        return done(null, false);
    }
));

exports.isAuthenticated = passport.authenticate('basic', { session : false });
