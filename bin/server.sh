#!/bin/bash

PRODUCTION="production"
DEVELOPMENT="development"
TEST="test"

LOGS_DIR="../logs/"
SECUTITY_DIR="../security/"
SSL_KEY="couchpotato.key"
SSL_CSR="couchpotato.csr"
SSL_CERTIFICATE="couchpotato.crt"

VALID_INPUT=false

if [[ $# = 0 ]];
then
    echo "Use argument -e to select the runing environment. usage: ./server.sh -e (production|development|test)"
    exit 1
fi

# Check if logs folder exists, if not, create one.
if [ ! -d "$LOGS_DIR" ];
then
    mkdir "$LOGS_DIR"
fi

# Check if security folder exists, if not, create one and the certificates
if [ ! -d "$SECUTITY_DIR" ];
then
    mkdir "$SECUTITY_DIR"
fi

if [ -d "$SECUTITY_DIR" ];
then
    KEY="$SECUTITY_DIR""$SSL_KEY"
    CERT="$SECUTITY_DIR""$SSL_CERTIFICATE"
    if [ ! -f "$KEY" ];
    then
        openssl genrsa -des3 -out "$SECUTITY_DIR""$SSL_KEY" 1024
    fi
    if [ ! -f "$CERT" ];
    then
        openssl req -new -key "$SECUTITY_DIR""$SSL_KEY" -out "$SECUTITY_DIR""$SSL_CSR"
        openssl x509 -req -days 365 -in "$SECUTITY_DIR""$SSL_CSR" -signkey "$SECUTITY_DIR""$SSL_KEY" -out "$SECUTITY_DIR""$SSL_CERTIFICATE"
    fi
fi

while [[ $# > 0 ]]
do
key="$1"

case $key in
    -e)
    ENV="$2"
    shift

    if [ "$ENV" == "$PRODUCTION" ];
    then
        echo "Production environment selected!"

    elif [ "$ENV" == "$DEVELOPMENT" ];
    then
        echo "Development environment selected!"

    elif [ "$ENV" == "$TEST" ];
    then
        echo "Test environment selected!"

    else
        echo "No valid environment selected! please use (production|development|test)!"
        exit 1
    fi

    VALID_INPUT=true
    ;;
esac

shift # past argument or value
done

# Set Node.js enviroment variable
export NODE_ENV=${ENV}

# Start application
if ! $VALID_INPUT ; then
    echo "Invalid parameter found!"
else
    cd ../ && node server.js
fi
