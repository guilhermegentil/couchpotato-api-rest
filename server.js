/**
 * Main application.
 *
 * @author Guilherme G. Amaral (ggamaral@gmail.com)
 */

// Check if the environment variable were defined, if not, close the application
if(!process.env.NODE_ENV){
    logger.error("Environment variable NODE_ENV is %s, shoud be (development|test|production)", process.env.NODE_ENV);
    process.exit(1);
}

var logger = require('./utils/logger');

// Compression middleware, required to use gziped communication
var compress = require('compression');

// Express 4 framework
var express = require('express');

// File system access
var fs = require('fs');

// Session management required by OAuth2orize
var session = require('express-session');

// API authentication and authorization library
var passport = require('passport');

// Configuration management
var config = require('config');

// Redis key-value in-memory database to store API authorization token information
var redis = require('redis');

// Https listener
var https = require('https');
var security_folder = config.get('server.security.location');
var privateKey  = fs.readFileSync(security_folder + config.get('server.security.ssl_key'), 'utf8');
var certificate = fs.readFileSync(security_folder + config.get('server.security.ssl_certificate'), 'utf8');
var credentials = { key: privateKey, cert: certificate }

var server_port = Number(process.env.COUCHPOTATO_API_SERVER_PORT || config.get('server.port'));

// Routes
var api = require('./routes/api');

// Create express application
logger.info("Initializing server in %s environment", process.env.NODE_ENV);
var app = express();

// Enable compression
app.use(compress());

// Use session support required by OAuth2orize library
app.use(session({
    secret: 'Secret Key',
    saveUninitialized: true,
    resave: true
}));

// Enable passport authentication framework
app.use(passport.initialize());

// Define routes
app.use('/api', api);

// Initialize Redis connection
var redisClient;
initRedis(redisClient);

// Start the server
https.createServer(credentials, app).listen(server_port);
logger.info("Server started on port %d, with PID %d, have fun!", server_port, process.pid);

/**
 * Initialize Redis
 */
function initRedis(redisClient) {
    // Connect to Redis
    var redisHost = config.get('redis.host');
    var redisPort = config.get('redis.port');
    var redisPassword = config.get('redis.password');

    logger.info("Connecting to Redis on endpoint: %s", redisHost + ":" + redisPort);
    var redisClient = redis.createClient(redisPort, redisHost);

    logger.info("Autenticating...");
    redisClient.auth(redisPassword);

    redisClient.on('connect', function() {
        logger.info("Connected to Redis!");
    });
    redisClient.on('error', function(err){
        logger.error("Failed to connect to Radis on %s, error: %s", redisHost, err);
    });
}
