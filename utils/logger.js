/**
 * Custom logger configuration
 *
 * @author Guilherme G. Amaral (ggamaral@gmail.com)
 */
var winston = require('winston');
var moment = require('moment');
var config = require('config');

var filename = config.get('server.logger.location') + config.get('server.logger.filename');

winston.emitErrs = true;

var logger = new winston.Logger({
    transports: [
        new winston.transports.DailyRotateFile({
            level: 'debug',
            filename: filename,
            handleExceptions: true,
            json: false,
            maxsize: 5242880, //5MB
            maxFiles: 10,
            colorize: false,
            timestamp: function() {
                return moment().format();
            },
            showLevel: true,
            tailable: true,
            zippedArchive: true
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true,
            timestamp: function() {
                return moment().format();
            },
            showLevel: true
        })
    ],
    exitOnError: false
});

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};

